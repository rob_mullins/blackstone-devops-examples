###########################################################################
# EBS Snapshot Manager Lambda Function
#
# This function is responsible for deleting the oldest automatic EBS volume 
# snapshots that have exceeded the defined `limit`. For example, if a
# volume's snapshots have a defined limit of 7, and there are currently 10 snapshots
# then this function will delete the 3 oldest snaphosts, so that the current number
# of snapshots is equal to the defined limit.
#
# This function is responsible for also creating defined `tags` for the
# snapshots, since tags such as Name & Description are not automatically
# created when a snapshot is created.
#
# @author Rob Mullins
# @version 0.1.0
###########################################################################

# Import Dependencies
import boto3 # External - AWS

class SnapshotManager:

  # Define Class Variables
  ec2_client = None
  ec2_resource = None

  def __init__(self):
    """
    Constructor - Instantiate the boto3 ec2.Client and ec2 resource objects.
    """
    # Set class variables
    self.ec2_client = boto3.client('ec2')
    self.ec2_resource = boto3.resource('ec2')

  def get_snapshots_by_volume_id(self, volume_id):
    """
    Gets all snapshots for a volume by the volume's ID.

    @param volume_id {string} - The volume ID to get snapshots for.
    @raises Exception if unable to get the volume's snapshots.
    @returns {dict[]} - List containing the snapshot IDs and their associated creation times.
    """
    # Validate arguments
    if not isinstance(volume_id, str):
      raise TypeError(
        'get_snapshots_by_volume_id() called without valid string for `volume_id` argument.'
      )

    # Get the snapshots
    try:
      describe_snapshots_response = self.ec2_client.describe_snapshots(
        Filters=[
          {
            'Name': 'volume-id',
            'Values': [
              volume_id
            ]
          }
        ]
      )
    except Exception as error:
      raise Exception(
        'Unexpected AWS error while calling describe_snapshots() in get_snapshots_by_volume_id() '
        +' - Error: {0}'.format(error)
      )

    # If we are able to parse the `Snapshots` array from the AWS response...
    if 'Snapshots' in describe_snapshots_response:
      snapshots = describe_snapshots_response.get('Snapshots')
      snapshot_data = [] # Init empty array to hold all returned snapshot IDs & their startTime

      # Iterate through each snapshot and push its ID to an array
      for snapshot in snapshots:
        if 'SnapshotId' in snapshot and 'StartTime' in snapshot:
          snapshot_data.append({'id': snapshot['SnapshotId'], 'created_datetime': snapshot['StartTime']})
    else:
      raise Exception(
        'get_snapshots_by_volume_id() received an unexepected response when calling '
        + 'describe_snapshots() Response: {0}'.format(describe_snapshots_response)
      )

    return snapshot_data

  def create_snapshot_tags(self, snapshot_id, new_tags):
    """
    Create tags for a snapshot if they don't already exist.

    @param snapshot_id {string} - The ID of the snapshot to create tags for.
    @param tags {dict[]} - List of dictionary of the tags to create for the snapshot.
    @raises Exception - If unable to create the tags.
    @returns void
    """
    # Validate arguments
    if not isinstance(snapshot_id, str):
      raise TypeError(
        'create_snapshot_tags() called without valid string for `snapshot_id` argument.'
      )
    if not isinstance(new_tags, list):
      raise TypeError('create_snapshot_tags() called without valid list for `tags` argument.')
    for new_snapshot_tag in new_tags:
      if not isinstance(new_snapshot_tag, dict):
        raise TypeError(
          'create_snapshot_tags() called without valid dictionary in '
          + '`new_snapshot_tags` list argument.'
        )

    # Get the current tags for the snapshot
    try:
      current_tags = self.get_snapshot_tags(snapshot_id)
      new_tags = self.remove_duplicate_tags(current_tags, new_tags)
    except Exception as error:
      raise Exception('create_snapshot_tags() was unable to get tags - {0}'.format(error))
    # Create the new tags for the snapshot
    try:
      if new_tags:
        self.ec2_client.create_tags(
          Resources=[
            snapshot_id
          ],
          Tags=new_tags
        )
    except Exception as error:
      raise Exception(
        'create_snapshot_tags() was unable to create tags for snapshot '
        + '{0} - {1}'.format(snapshot_id, error)
      )

  def remove_duplicate_tags(self, existing_tags, new_tags):
    """
    Removes tags from new_tags that already exist in existing_tags

    @param existing_tags {dict[]}
    @param new_tags {dict[]}
    @return {dict[]}
    """
    for index, new_tag in enumerate(new_tags):
      new_tag_key = new_tag.get('Key')
      new_tag_value = new_tag.get('Value')

      for existing_tag in existing_tags:
        existing_tag_key = existing_tag.get('Key')
        existing_tag_value = existing_tag.get('Value')

        if existing_tag_key == new_tag_key:
          if existing_tag_value == new_tag_value:
            del new_tags[index]

    return new_tags

  def get_snapshot_tags(self, snapshot_id):
    """
    Get all tags for a specified snapshot.

    @param snapshot_id {string} - The ID of the snapshot to get tags for.
    @raises Exception - If unable to get tags.
    @returns void
    """
    # Validate arguments
    if not isinstance(snapshot_id, str):
      raise TypeError('get_snapshot_tags() called without valid string for `snapshot_id` argument.')

    # Get the tags
    try:
      describe_tags_response = self.ec2_client.describe_tags(
        Filters=[
          {
            'Name': 'resource-id',
            'Values': [
              snapshot_id
            ]
          }
        ]
      )
    except Exception as error:
      raise Exception(
        'Unexpected AWS error while calling describe_tags() in get_snapshot_tags() '
        +' - Error: {0}'.format(error)
      )

    # If we are able to parse the `Tags` array from the AWS response...
    if 'Tags' in describe_tags_response:
      tags = describe_tags_response.get('Tags')

      # Delete irrelevant data from tags array
      for tag in tags:
        for key in list(tag): # tag is a dictionary, iterate through its keys
          if str(key) != 'Key' and str(key) != 'Value':
            del tag[key]
    else:
      raise Exception(
        'get_snapshot_tags() received an unexepected response when calling describe_tags() '
        + 'Response: {0}'.format(describe_tags_response)
      )

    return tags

  def delete_snapshot_by_id(self, snapshot_id):
    """
    Deletes an EBS Snapshot

    @param snapshot_id {string} - The ID of the snapshot to delete.
    @raises Exception if unable to delete the snapshot.
    @returns void
    """
    # Validate arguments
    if not isinstance(snapshot_id, str):
      raise TypeError(
        'delete_snapshot_by_id() called without valid string for `snapshot_id` argument.'
      )

    # Delete the snapshot
    try:
      snapshot = self.ec2_resource.Snapshot(snapshot_id)
      snapshot.delete()
    except Exception as error:
      raise Exception(
        'Unexpected AWS error when attempting to delete snapshot: {0} '.format(snapshot_id)
        + 'in delete_snapshot_by_id() - Error: {1}'.format(error)
      )
