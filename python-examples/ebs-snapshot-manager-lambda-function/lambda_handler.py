# Import Dependencies
from datetime import datetime, timezone, timedelta
from dateutil.tz import tzutc
import json
import os
from .ebs_snapshot_manager import SnapshotManager
from .custom_exceptions import *

# Declare Global Config Constants
BACKUPS_ENV_VAR = 'BACKUPS'
SNAPSHOT_TAGS_ENV_VAR = '{0}_TAGS'

# Globally instantiate SnapshotManager
# Warning - this is okay to do this here because the
# SnapshotManager singleton doesn't set customer-unique values.
# but if it was modified to do so, you would need to instantiate 
# a unique instance within the handler to avoid data collisions. 
snapshot_manager = SnapshotManager() 


def handler(event, context):
  """
  Lambda Entry Function
  """
  errors = []

  backups = get_backups_from_env_var(BACKUPS_ENV_VAR)
  for backup in backups:
    # Delete expired snapshots
    try:
      backup_data = get_backup_data_from_env_var(backup)

      snapshots = snapshot_manager.get_snapshots_by_volume_id(backup_data['volume_id'])
      snapshot_deletion_results = delete_expired_snapshots(snapshots, backup_data['limit'])

      # Create list of failed snapshot deletions & and merge deletion errors into `errors`
      if snapshot_deletion_results['failed_deletions']:
        failed_snapshot_deletion_errors = []
        failed_snapshot_deletions = []
        for failed_snapshot_deletion in enumerate(snapshot_deletion_results['failed_deletions']):
          failed_snapshot_deletions.append(failed_snapshot_deletion['snapshot_id'])
          failed_snapshot_deletion_errors.append(failed_snapshot_deletion['error'])
        errors = errors + failed_snapshot_deletion_errors
    except Exception as error:
      errors.append(error)
    
    # Tag snaphosts
    try:
      snapshot_tags = get_snapshot_tags_from_env_var(SNAPSHOT_TAGS_ENV_VAR.format(backup))
      if snapshot_tags is not None:
        tag_results = tag_snapshots(snapshots, snapshot_tags)
        errors = errors + tag_results['errors'] # Merge tag_errors into `errors`
    except Exception as error:
      errors.append(error)

  # If any errors, print them, and end Lambda with Error status
  if errors:
    print('There were errors during execution, printing them...')
    for error in errors:
      print(error)
    # End Lambda Execution with Error Status
    raise Exception('Errors encountered during execution.')

def get_backups_from_env_var(env_var):
  backups = os.environ.get(env_var) # String of comma seperated values

  if backups is None:
    raise EnvVarNotSetError('Error: {0} environment variable not set.')
  elif ',' not in backups and ' ' not in backups:

    return [backups] # Just one backup, not a CDL

  elif ',' not in backups or ' ' in backups:
    raise ValueNotCommaSeperatedList(
      'Error: {0} environment variable value '.format(backups)
      + 'is not a valid comma seperated string.'
    )
  backups = backups.split(',')

  return backups

def get_backup_data_from_env_var(env_var):
  backup_data = os.environ.get(env_var)
  if backup_data is None:
    raise EnvVarNotSetError('{0} environment variable not set.'.format(env_var))
  try:
    backup_data = json.loads(backup_data)
    if 'volume_id' not in backup_data or 'limit' not in backup_data:
      raise InvalidJsonSchemaError('{0} JSON missing required properties.'.format(env_var))
    elif not isinstance(backup_data['volume_id'], str):
      raise TypeNotStringError('volume_id is not a string')
    elif not isinstance(backup_data['limit'], int):
      raise TypeNotNumberError('limit is not a number')
    elif backup_data['limit'] <= 0:
      raise ValueTooSmallError('limit is not a number > 0')
  except json.JSONDecodeError:
    raise StringNotJsonError(
      '{0} environment variable value is not '.format(env_var)
      + 'a valid JSON string.'
    )

  return backup_data

def delete_expired_snapshots(snapshots, limit):
  snapshots = sorted(
    snapshots, key=lambda k: k['created_datetime'], reverse=True
  )

  failed_deletions = []
  successful_deletions = []
  for snapshot in snapshots[limit:]:    
    snapshot_id = snapshot['id']
    try:
      snapshot_manager.delete_snapshot_by_id(snapshot_id)
      successful_deletions.append(snapshot_id)
    except Exception as error:
      failed_deletions.append({'snapshot_id': snapshot_id, 'error': error})

  return {'failed_deletions': failed_deletions, 'successful_deletions': successful_deletions}

def tag_snapshots(snapshots, snapshot_tags):
  errors = []

  try:
    ec2_tags_dto = build_ec2_tags_dto(snapshot_tags)
  except TypeError as error:
    errors.append('Failed to build the ec2 tags DTO - snapshot_tags is not a valid dictionary')

  if ec2_tags_dto:
    for snapshot in snapshots:
      try:
        snapshot_id = snapshot['id']
        snapshot_manager.create_snapshot_tags(snapshot_id, ec2_tags_dto)
      except Exception as error:
        errors.append('Failed to tag snapshot {0} with error {1}'.format(snapshot_id, error))

  return {'errors': errors}

def build_ec2_tags_dto(snapshot_tags):
  if not isinstance(snapshot_tags, dict):
    raise TypeNotDictError

  ec2_tags_dto = []
  for snapshot_tag_key, snapshot_tag_value in snapshot_tags.items():
    ec2_tags_dto.append({'Key': snapshot_tag_key, 'Value': snapshot_tag_value})

  return ec2_tags_dto

def get_snapshot_tags_from_env_var(env_var):
  snapshot_tags = os.environ.get(env_var)
  
  if snapshot_tags is not None:
    try:
      snapshot_tags = json.loads(snapshot_tags)
    except json.JSONDecodeError:
      raise StringNotJsonError(
        '{0} environment variable value is '.format(env_var)
        + 'not a valid JSON string.'
      )
  
  return snapshot_tags
