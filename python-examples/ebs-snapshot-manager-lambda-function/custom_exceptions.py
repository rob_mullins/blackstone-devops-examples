class Error(Exception):
  pass

class StringNotJsonError(Error):
  pass

class InvalidJsonSchemaError(Error):
  pass

class EnvVarNotSetError(Error):
  pass

class ValueNotCommaSeperatedList(Error):
  pass

class TypeNotStringError(Error):
  pass

class TypeNotNumberError(Error):
  pass

class ValueTooSmallError(Error):
   pass

class TypeNotDictError(Error):
  pass


