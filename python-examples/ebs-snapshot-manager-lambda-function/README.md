# Summary

This Lambda function is responsible for 
  1. Deleting the oldest EBS Volume Snapshots that have exceeded the defined `limit`,  
  2. Setting tags, if defined, for the Snanpshots. 

# How It Works

The Lambda function manages snapshots which are defined as function-level environment variables. 

The environment variable syntax for managing backups is as so:
  1. BACKUPS="{{BACKUP_NAME_1}},{{BACKUP_NAME_2}}
  2. {{BACKUP_NAME_1}}='{"volume_id": "the snapshot parent EBS volume id", "limit": max-number-of-snapshots}'
  3. {{BACK_NAME_1}}_TAGS='{"SomeTagKey1": "SomeValue1", "SomeTagKey2": "SomeValue2"}'
  4. {{BACKUP_NAME_2}}='{"volume_id": "the snapshot parent EBS volume id", "limit": max-number-of-snapshots}'
  5. {{BACK_NAME_2}}_TAGS='{"SomeTagKey1": "SomeValue1", "SomeTagKey2": "SomeValue2"}'

So for example, let's say we want to retain a maximum 14 snapshots for our Tableau server, and set a 'Name' tag on
its snapshots. In the serverless.yml file, in the function definition, we would define the following environment
variables like so...

    environment:
      BACKUPS: 'TABLEAU'
      TABLEAU: '{"volume_id": "vol-0c656439a6c3e2e34", "limit": 14}'
      TABLEAU_TAGS: '{"Name": "Tableau Backup"}'

Each value in the `BACKUPS` environment variable must map to another environment variable of the same name
with its value set to a JSON string containing the `volume_id` and `limit` properties.

Note that the {{BACKUP}}_TAGS environment variable is optional.

As another example, if we wanted to manage backups for two services - Tableau & AWX, the function level
environment variables would look like so...

    environment:
      BACKUPS: 'TABLEAU,AWX'
      TABLEAU: '{"volume_id": "vol-0c656439a6c3e2e34", "limit": 14}'
      TABLEAU_TAGS: '{"Name": "Tableau Backup"}'
      AWX: '{"volume_id": "vol-xxxxxxxxxxxxx", "limit": 7}'
      AWX_TAGS: '{"Name": "AWX Backup"}'

# Important Notes
If you decide to add additional volumes to manage backups for, you will need to update the policy attached
to the Lambda's IAM role - `arn:aws:iam::${account_id}:role/{role_name}`. Specifically,
you will need to update the policy to include the volume_id in it. 
