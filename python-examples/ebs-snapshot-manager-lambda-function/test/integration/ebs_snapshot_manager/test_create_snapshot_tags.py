# Make sure PYTHONPATH environment variable is set to the project root absolute path. 
import pytest
from functions.ebs_snapshot_manager.ebs_snapshot_manager import SnapshotManager

############################################################################################################ 
# DEFINE CONFIG VALUES HERE
############################################################################################################ 

# The snapshot to create tags for
snapshot_id = 'snap-036d72b10887d410e' # `snap-036d72b10887d410e` is a test snapshot just for this test. 

# The tags we want to create
tags = [] # Init empty list to hold tag dictionaries
tags.append({'Key': 'Name', 'Value': 'ebs_snapshot_manager test snapshot'}) # DON'T CHANGE THIS ONE
tags.append({'Key': 'Key1', 'Value': 'Value1'}) # Change these to new values
tags.append({'Key': 'Key2', 'Value': 'Value2'}) # Change these to new values

############################################################################################################ 
# END CONFIG
############################################################################################################ 

def describe_create_snapshot_tags():
  snapshot_manager = SnapshotManager()

  def it_should_create_the_defined_tags():
    snapshot_manager.create_snapshot_tags(snapshot_id, tags)

    actual = snapshot_manager.get_snapshot_tags(snapshot_id)
    expected = [
      {'Key': 'Key1', 'Value': 'Value1'}, 
      {'Key': 'Key2', 'Value': 'Value2'}, 
      {'Key': 'Name', 'Value': 'ebs_snapshot_manager test snapshot'}
    ]
    
    assert actual == expected

# if __name__ == '__main__':
#   main()