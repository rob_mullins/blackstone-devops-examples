# Make sure PYTHONPATH environment variable is set to the project root absolute path. 
import pytest
from functions.ebs_snapshot_manager.ebs_snapshot_manager import SnapshotManager

############################################################################################################ 
# DEFINE CONFIG VALUES HERE
############################################################################################################ 

# The snapshot to get tags for
snapshot_id = 'snap-036d72b10887d410e' # `snap-036d72b10887d410e` is a test snapshot just for this test. 

############################################################################################################ 
# END CONFIG
############################################################################################################ 

def describe_get_snapshot_tags():
  snapshot_manager = SnapshotManager()

  def it_should_get_the_snapshot_tags():
    actual = snapshot_manager.get_snapshot_tags(snapshot_id)
    expected = [
      {'Key': 'Key1', 'Value': 'Value1'}, 
      {'Key': 'Key2', 'Value': 'Value2'}, 
      {'Key': 'Name', 'Value': 'ebs_snapshot_manager test snapshot'}
    ]

    assert actual == expected
