# Make sure PYTHONPATH environment variable is set to the project root absolute path. 
import pytest
import datetime
from dateutil.tz import tzutc
from functions.ebs_snapshot_manager.ebs_snapshot_manager import SnapshotManager

############################################################################################################ 
# DEFINE CONFIG VALUES HERE
############################################################################################################ 

# The volume to get snapshots for
volume_id = 'vol-043a1a4cb93b04538' # test volume with 1 snapshot

############################################################################################################ 
# END CONFIG
############################################################################################################ 

def describe_get_snapshots_by_volume_id():
  snapshot_manager = SnapshotManager()
 
  def it_should_return_the_snapshots():
    actual = snapshot_manager.get_snapshots_by_volume_id(volume_id)
    expected = [
      {
        'id': 'snap-036d72b10887d410e', 
        'created_datetime': datetime.datetime(2019, 7, 9, 19, 21, 22, 623000, tzinfo=tzutc())
      }
    ]

    assert actual == expected
    