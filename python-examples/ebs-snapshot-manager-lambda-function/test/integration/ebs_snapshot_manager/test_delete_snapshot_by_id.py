# Make sure PYTHONPATH environment variable is set to the project root absolute path. 
import pytest
import time
import boto3
from botocore.exceptions import ClientError
from functions.ebs_snapshot_manager.ebs_snapshot_manager import SnapshotManager

############################################################################################################ 
# DEFINE CONFIG VALUES HERE
############################################################################################################ 

# The volume to create a temporary snapshot for
volume_id = 'vol-043a1a4cb93b04538' # `vol-043a1a4cb93b04538` is an unused 2GiB volume for Kubernetes

############################################################################################################ 
# END CONFIG
############################################################################################################ 

def describe_delete_snapshot_by_id():
  snapshot_manager = SnapshotManager()
  ec2_client = boto3.client('ec2')

  @pytest.fixture(scope='session', autouse=True)
  def temporary_snapshot_id():
    # Will be executed before the first test
    create_snapshot_response = ec2_client.create_snapshot(VolumeId=volume_id)
    temporary_snapshot_id = create_snapshot_response.get('SnapshotId')
    time.sleep(30) # Ample time for the snapshot to get created and become 'ACTIVE'
    #temporary_snapshot_id = create_temporary_snapshot(volume_id)
    yield temporary_snapshot_id

  def it_should_delete_the_snapshot(temporary_snapshot_id):
    #with pytest.raises(ClientError):
    snapshot_manager.delete_snapshot_by_id(temporary_snapshot_id)
    try:
      ec2_client.describe_snapshots(
        SnapshotIds=[temporary_snapshot_id]
      )
    except ClientError as error:
      assert error.response['Error']['Code'] == 'InvalidSnapshot.NotFound'

  # def create_temporary_snapshot(volume_id):
  #   try:
  #     create_snapshot_response = ec2_client.create_snapshot(VolumeId=volume_id)
  #     temporary_snapshot_id = create_snapshot_response.get('SnapshotId')
  #     time.sleep(30) # Ample time for the snapshot to get created and become 'ACTIVE'

  #     return temporary_snapshot_id

  #   except Exception as error:
  #     raise Exception(
  #       'Unable to create temporary snapshot for volume: {0} - Error: {1}'.format(volume_id, error)
  #     )
