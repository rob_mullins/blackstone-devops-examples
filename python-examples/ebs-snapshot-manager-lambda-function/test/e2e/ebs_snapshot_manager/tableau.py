import os
from functions.ebs_snapshot_manager import lambda_handler

if __name__ == "__main__":
  os.environ['BACKUPS'] = 'TABLEAU'
  os.environ['TABLEAU'] = '{"volume_id": "vol-0c656439a6c3e2e34", "limit": 14}'
  os.environ['TABLEAU_TAGS'] ='{"Name": "Tableau Backup"}' 

  lambda_handler.handler(None, None)