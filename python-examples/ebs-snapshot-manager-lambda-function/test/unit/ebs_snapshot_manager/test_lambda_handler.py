import pytest
from functions.ebs_snapshot_manager import lambda_handler
from functions.ebs_snapshot_manager.custom_exceptions import *

# get_backups_from_env_var()
def describe_get_backups_from_env_var():

  def it_should_raise_EnvVarNotSetError_if_env_var_not_set():
    env_var = 'BACKUPS'
    with pytest.raises(EnvVarNotSetError):
      assert lambda_handler.get_backups_from_env_var(env_var)
  
  def it_should_raise_ValueNotCommaSeperatedList_if_env_var_value_not_comma_delimited(monkeypatch):
    env_var = 'BACKUPS'
    monkeypatch.setenv(env_var, 'BACKUP1 BACKUP2 BACKUP3')

    with pytest.raises(ValueNotCommaSeperatedList):
      assert lambda_handler.get_backups_from_env_var(env_var)

  def it_should_split_env_var_arg(monkeypatch):
    env_var = 'BACKUPS'
    monkeypatch.setenv(env_var, 'BACKUP1,BACKUP2,BACKUP3')

    expected_result = ['BACKUP1', 'BACKUP2', 'BACKUP3']
    actual_result = lambda_handler.get_backups_from_env_var(env_var)

    assert expected_result == actual_result

# get_backup_data_from_env_var()
def describe_get_backup_data_from_env_var():

  def it_should_raise_EnvVarNotSetError_if_env_var_not_set():
    env_var = 'BACKUP1'
    with pytest.raises(EnvVarNotSetError):
      assert lambda_handler.get_backup_data_from_env_var(env_var)

  def it_should_raise_ValueNotJsonError_if_env_var_value_not_json(monkeypatch):
    env_var = 'BACKUP1'
    monkeypatch.setenv(env_var, '{"Key: "Value"}')

    with pytest.raises(StringNotJsonError):
      assert lambda_handler.get_backup_data_from_env_var(env_var)

  def it_should_raise_InvalidJsonSchemaError_if_invalid_json_schema(monkeypatch):
    env_var = 'BACKUP1'
    monkeypatch.setenv(env_var, '{"Key": "Value"}')

    with pytest.raises(InvalidJsonSchemaError):
      assert lambda_handler.get_backup_data_from_env_var(env_var)

  def it_should_raise_ValueNotStringError_if_volume_id_property_value_not_string(monkeypatch):
    env_var = 'BACKUP1'

    json_property = 'volume_id'
    json_value = 1
    monkeypatch.setenv(env_var, '{{"{0}": {1}, "limit": 1}}'.format(json_property, json_value))

    with pytest.raises(TypeNotStringError):
      assert lambda_handler.get_backup_data_from_env_var(env_var)

  def it_should_raise_ValueNotNumberError_if_limit_property_value_not_number(monkeypatch):
    env_var = 'BACKUP1'
    
    json_property = 'limit'
    json_value = 14
    monkeypatch.setenv(env_var, '{{"{0}": "{1}", "volume_id": "str"}}'.format(json_property, json_value))

    with pytest.raises(TypeNotNumberError):
      assert lambda_handler.get_backup_data_from_env_var(env_var)

  def it_should_raise_ValueTooSmallError_if_limit_property_value_lte_zero(monkeypatch):
    env_var = 'BACKUP1'
    
    json_property = 'limit'
    json_value = 0
    monkeypatch.setenv(env_var, '{{"{0}": {1}, "volume_id": "str"}}'.format(json_property, json_value))

    with pytest.raises(ValueTooSmallError):
      assert lambda_handler.get_backup_data_from_env_var(env_var)

  def it_should_return_backup_data_as_dict(monkeypatch):
    env_var = 'BACKUP1'
    monkeypatch.setenv(env_var, '{"volume_id": "str", "limit": 1}')

    actual_result = lambda_handler.get_backup_data_from_env_var(env_var)
    assert isinstance(actual_result, dict)
  
# describe_build_ec2_tags_dto()
def describe_build_ec2_tags_dto():

  def it_should_raise_TypeError_if_first_arg_not_dict():
    first_arg = "not a dictionary"
    with pytest.raises(TypeNotDictError):
      assert lambda_handler.build_ec2_tags_dto(first_arg)

  def it_should_return_ec2_tags_dto():
    tags = {'Tag1': 'Val1', 'Tag2': 'Val2'}

    expected_result = [
      {'Key': 'Tag1', 'Value': 'Val1'},
      {'Key': 'Tag2', 'Value': 'Val2'}
    ]
    actual_result = lambda_handler.build_ec2_tags_dto(tags)

    assert expected_result == actual_result

# get_snapshot_tags_from_env_var()
def describe_get_snapshot_tags_from_env_var():

  def it_should_raise_StringNotJsonError_if_env_var_value_not_json(monkeypatch):
    env_var = 'BACKUP1_TAGS'
    monkeypatch.setenv(env_var, '{"Key: "Value"}')

    with pytest.raises(StringNotJsonError):
      assert lambda_handler.get_snapshot_tags_from_env_var(env_var) 

  def it_should_return_snapshot_tags_as_dict(monkeypatch):
    env_var = 'BACKUP1_TAGS'
    monkeypatch.setenv(env_var, '{"Tag1": "Val1", "Tag2": "Val2"}')

    actual_result = lambda_handler.get_snapshot_tags_from_env_var(env_var)
    assert isinstance(actual_result, dict)

