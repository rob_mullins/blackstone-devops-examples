# Welcome

### Rob Mullins selected DevOps Examples for Blackstone review. 

I've put together this repo of several projects where I was the sole author, for purposes of simply demonstrating my abilities in previous projects I worked on. I hope these examples can help complement any areas of the interview where I may not have performed as well on.

This repo is purely for reference purposes. 
