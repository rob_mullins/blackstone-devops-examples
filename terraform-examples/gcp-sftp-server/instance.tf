data "google_compute_image" "that_filemage_public_image" {
  family  = "filemage-ubuntu"
  project = "filemage-public"
}

resource "google_compute_instance_template" "this_sftp_server_template" {
  name_prefix = "${local.resource_prefix}-template-"
  machine_type = "f1-micro"
  tags         = [
    "${var.customer}-sftp-instance"
  ]

  disk {
    boot = true
    source_image = data.google_compute_image.that_filemage_public_image.self_link
  }

  network_interface {
    network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.vpc}"
  }

  metadata = {
    startup-script = templatefile("${path.module}/templates/instance-startup-script.sh.tftpl", {
      filemage_admin_email = var.filemage_admin_email
      filemage_admin_password = var.filemage_admin_password
      postgres_cluster_hostname = "disabled"
      #postgres_cluster_hostname = data.google_sql_database_instance.that_shared_postgres_cluster.private_ip_address
      sftp_bucket = local.this_sftp_bucket_name
      customer = var.customer
      customer_public_ssh_key = var.customer_public_ssh_key
    })
  }

  service_account {
    email  = google_service_account.vmsa.email
    scopes = ["cloud-platform"]
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "random_string" "this_sftp_server_template" {
  keepers = {
    id = google_compute_instance_template.this_sftp_server_template.id
  }
  length = 3
  upper = false
  special = false
}




resource "google_compute_instance_from_template" "this_sftp_server" {
  # Add random string to support 'create_before_destoy' & multiple instances
  name = "${local.resource_prefix}-instance-${random_string.this_sftp_server_template.id}"
  source_instance_template = random_string.this_sftp_server_template.keepers.id  
}




# Target Instance (below) cant be edited or changed when its already attached to a Forwarder.
# So have it change its name, resulting in a new resource, whenever the instance/template is changed,
# which causes it (the target instance) to also change).
# The dependency is: Forwarder needs TargetInstance, which needs InstanceFromTemplate, which needs Template.
resource "random_string" "this_sftp_server" {
  keepers = {
    id = google_compute_instance_from_template.this_sftp_server.id
  }
  length = 3
  upper = false
  special = false
}

resource "google_compute_target_instance" "this_sftp_server_group" {
  name     = "${local.resource_prefix}-target-instance-${random_string.this_sftp_server.id}"
  instance = random_string.this_sftp_server.keepers.id
  
  # Create a new one before destroying the existing, so we don't suddenly break the old forwarder
  lifecycle {
    create_before_destroy = true
  }
}
