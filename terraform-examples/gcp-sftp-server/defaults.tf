locals {
  environment = "${var.environment}"

  common_tags = {
    owner = "data"
    purpose = "sftp"
    environment = "${var.environment}"
  }

  environment_short = substr(var.environment, 0, 1)
  resource_prefix = "eiq-sftp-${var.customer}-${local.environment_short}"
  this_sftp_bucket_name = "${local.resource_prefix}-storage"
}