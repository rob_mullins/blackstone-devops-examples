output "inbound_ip" {
  value = google_compute_address.this_static_ip.address
}

output "instance_id" {
  value = google_compute_instance_from_template.this_sftp_server.id 
}

output "customer_bucket_name" {
  value = local.this_sftp_bucket_name
}