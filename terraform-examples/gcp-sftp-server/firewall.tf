resource "google_compute_firewall" "allow_ingress_from_customer" {
  name          = "${local.resource_prefix}-firewall-allow-ingress-from-customer"
  network       = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.vpc}"
  direction     = "INGRESS"
  priority      = "1001"
  source_ranges = var.customer_ip_list

  allow {
    protocol = "tcp"
    ports    = ["22", "443"]
  }

  target_tags = ["${var.customer}-sftp-instance"]
  #destination_ranges = [ join("", [google_compute_address.this_static_ip.address, "/32"]) ]
}

resource "google_compute_firewall" "allow_ingress_from_eiq" {
  name          = "${local.resource_prefix}-firewall-allow-ingress-from-eiq"
  network       = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.vpc}"
  direction     = "INGRESS"
  priority      = "1002"
  source_ranges = var.eiq_ip_list

  allow {
    protocol = "tcp"
    ports    = ["22", "2222", "443", "8443"]
  }

  #destination_ranges = [ join("", [google_compute_address.this_static_ip.address, "/32"]) ]
  target_tags = ["${var.customer}-sftp-instance"]
}

resource "google_compute_firewall" "allow_ingress_from_iap" {
  name          = "${local.resource_prefix}-firewall-allow-ingress-from-iap"
  network       = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.vpc}"
  direction     = "INGRESS"
  priority      = "1003"
  source_ranges = ["35.235.240.0/20"]

  allow {
    protocol = "tcp"
    ports    = ["22", "2222"]
  }

  #destination_ranges = [ join("", [google_compute_address.this_static_ip.address, "/32"]) ]
  target_tags = ["${var.customer}-sftp-instance"]
}

resource "google_compute_firewall" "deny_ingress_from_unknown" {
  name          = "${local.resource_prefix}-sftp-firewall-deny-all"
  network       = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.vpc}"
  direction     = "INGRESS"
  priority      = "2000"
  source_ranges = ["0.0.0.0/0"]

  deny {
    protocol = "all"
  }

  target_tags = ["${var.customer}-sftp-instance"]
}