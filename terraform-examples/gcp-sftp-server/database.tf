resource "google_sql_database" "this_customers_database_in_that_shared_postgres_cluster" {
  count = var.postgres_cluster_instance_name == "DISABLED" ? 0 : 1
  name     = var.customer
  instance = var.postgres_cluster_instance_name
}

data "google_sql_database_instance" "that_shared_postgres_cluster" {
  count = var.postgres_cluster_instance_name == "DISABLED" ? 0 : 1
  name = var.postgres_cluster_hostname
}