resource "google_compute_address" "this_static_ip" {
  name         = "${local.resource_prefix}-ip-address"
  network_tier = "STANDARD"
}

resource "random_string" "this_sftp_server_group" {
  keepers = {
    id = google_compute_target_instance.this_sftp_server_group.id
  }
  length = 3
  upper = false
  special = false
}

resource "google_compute_forwarding_rule" "admin_portal" {
  count = 1
  name         = "${local.resource_prefix}-admin-portal-forwarding"
  ip_protocol  = "TCP"
  load_balancing_scheme = "EXTERNAL"
  target       = random_string.this_sftp_server_group.keepers.id
  port_range   = "8443"
  ip_address   = google_compute_address.this_static_ip.address
  network_tier = "STANDARD"
}

resource "google_compute_forwarding_rule" "customer_portal" {
  count = 0
  name         = "${local.resource_prefix}-portal-forwarding"
  ip_protocol  = "TCP"
  load_balancing_scheme = "EXTERNAL"
  target       = random_string.this_sftp_server_group.keepers.id
  port_range   = "443"
  ip_address   = google_compute_address.this_static_ip.address
  network_tier = "STANDARD"
}

resource "google_compute_forwarding_rule" "sftp" {
  count = 1
  name         = "${local.resource_prefix}-sftp-forwarding"
  ip_protocol  = "TCP"
  load_balancing_scheme = "EXTERNAL"
  target       = random_string.this_sftp_server_group.keepers.id
  port_range   = "22"
  ip_address   = google_compute_address.this_static_ip.address
  network_tier = "STANDARD"
}