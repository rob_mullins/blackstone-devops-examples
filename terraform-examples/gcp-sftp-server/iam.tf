resource "google_service_account" "vmsa" {
  account_id   = "${local.resource_prefix}-sa"
  display_name = "${local.resource_prefix}-sa"
}

resource "google_project_iam_custom_role" "vmsa_storage_access" {
  role_id     = "eiq_data_${var.customer}_${local.environment_short}_sftp_storage_role"
  title       = "${var.customer} ${local.environment} SFTP Storage Role"
  description = "${var.customer} ${local.environment} SFTP Storage Role"
  permissions = ["storage.objects.list", "storage.objects.create"]
}

resource "google_storage_bucket_iam_member" "vmsa" {
  bucket = local.this_sftp_bucket_name
  role = google_project_iam_custom_role.vmsa_storage_access.id
  member = "serviceAccount:${google_service_account.vmsa.email}"
}

# Temporarily allow read access to resources in 'condition' for instance start up
locals {
  temporary_15m_access = timeadd(timestamp(), "15m")
}
resource "google_project_iam_member" "vmsa_temp_secrets_access" {
  project = var.project
  role = "roles/secretmanager.secretAccessor"
  member = "serviceAccount:${google_service_account.vmsa.email}"

  condition {
    title       = "Temporary secrets access for SFTP ${var.customer}"
    expression  = join(" ", 
      [
        "resource.name.extract('/secrets/{name}/').startsWith('eiq-ops-iq-p-sftp-')", 
        "&& request.time < timestamp('${local.temporary_15m_access}')"
    ])
  }
}