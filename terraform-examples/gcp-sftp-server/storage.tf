resource "google_storage_bucket" "this" {
  name          = local.this_sftp_bucket_name
  location      = "US"
  force_destroy = true
}