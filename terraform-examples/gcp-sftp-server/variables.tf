variable "region" {
  type = string
  description = "The region we are working out of. Should be the same region as the VPC."
}

variable "vpc" {
  type = string
  description = "The name of the VPC to deploy resources into."
}

variable "zone" {
  type = string
  description = "The region value with the zone/location appeneded at end. Example: us-east1-b"
}

variable "project" {
  type = string
  description = "The name of the GCP project to work out of."
}

variable "eiq_ip_list" {
  type = list
  description = "List of IP addresses in CIDR format beloniging to EIQ that we need to allowlist."
  default = ["207.237.73.46/32"]
}

variable "customer" {
  type = string
  description = "The name fo the customer we are deploying SFTP for."
}

variable "customer_ip_list" {
  type = list
  description = "List of IP addresses in CIDR format that need to be allowed on port 22 for the customer to access SFTP."
}

variable "customer_public_ssh_key" {
  type = string
  description = "The customer's public SSH key to associate with the SFTP user to be created."
}

variable "filemage_admin_email" {
  type = string
  description = "The default FileMage Admin Email used for Admin logins."
  default = "devops@evolutioniq.com"
}

variable "filemage_admin_password" {
  type = string
  description = <<-EOF
    The default FileMage Admin password used for the very first Admin login,
    at which poinst the user is forced to reset.
  EOF
  default = "337200129dac322a2bcdadeeeac5283a"
}

variable "postgres_cluster_instance_name" {
  type = string
  default = "DISABLED"
  description = "The GCP cluster name of the shared SFTP Postgres cluster to use."
}

variable "postgres_cluster_hostname" {
  type = string 
  description = "The GCP cluster hostname of the shared SFTP Postgres cluster to use."
}

variable "environment" {
  type = string
  validation {
    condition     = var.environment == "develop" || var.environment == "staging" || var.environment == "production"
    error_message = "Input variable 'environment' must be one of ['develop', 'staging', 'production']."
  }
}